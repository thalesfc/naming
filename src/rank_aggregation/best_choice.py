import sys
sys.path.append('/Users/thalesfc/workspace/naming/src')

import globalNames
import loadData
from collections import defaultdict

# get files
def openFiles(name):
    submission = open(globalNames.PATH_SUBMISSIONS + "/" + name)
    feature = open(globalNames.PATH + "/results" + "/" + name)
    return submission, feature

 
rankings = [ openFiles('topPop'), openFiles('puresvd_5'), openFiles('puresvd_10'), openFiles('puresvd_15') ]

train = defaultdict(list)

# loading the performance of ever user in every rank
for _, ffeature in rankings:
    for line in ffeature:
        user, sscore = line.strip().split("\t")
        user_id = int(user)
        score = float(sscore)
        train[user_id].append(score) 

testUsers = loadData.loadTestUsers(globalNames.PATH_TESTUSERS)
out = open(globalNames.PATH_SUBMISSIONS + "/best_choice_v1.txt", 'w')

nu_count = 0 # new user count

for i, user in enumerate(testUsers):
    print i, user
    
    score_list = train[user]
    if not score_list:
        max_index = 0
#         print "$ NEW USER" , user
        nu_count += 1
    else:
        max_index = score_list.index(max(score_list))
    
    for index, tup in enumerate(rankings):
        sub, _ = tup
        name_list = sub.readline()
        if index == max_index:
            out.write(name_list)
            
out.close()
print "# New USERS", nu_count