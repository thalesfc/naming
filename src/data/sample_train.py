'''
    Create sample training based on test
'''
import loadData
import globalNames
from collections import defaultdict
import random

training, _, _ = loadData.loadData(globalNames.PATH + "/full_dataset/nameling.trainData")

train_dict = defaultdict(list)

count = 0
testNames = loadData.loadNameList(globalNames.PATH_NAMELIST)

for user, activity, name in training:
    count += 1
    if count % 10000 == 0:
        print count
        
    if name in testNames:
        if activity == 'ENTER_SEARCH':
            train_dict[user].append(name)
    
testUsers = loadData.loadTestUsers(globalNames.PATH + "/full_dataset/nameling.testUsers")

nnew_users = 0 # num new users
nspar_user = 0 # num sparse users

set_new_users = set()

for user in testUsers:
    if not user in train_dict:
        # print "$ NEW USER:", user
        set_new_users.add(user)
        nnew_users += 1
    else:
        utrain = train_dict[user] # list of names previously liked
        if len(utrain) < 2:
            # print "$ SPARSE USER", user, len(utrain)
            nspar_user += 1
        
print "# new users: ", nnew_users
print "# sparse users: ", nspar_user
print "# normal users: ", len(testUsers) - nnew_users - nspar_user

set_test_users = set(testUsers)
users_replace_new = set()

# selecting users to replace new users:
for user, nlist in train_dict.items():
    if len(nlist) == 2:
        if not user in set_test_users:
            users_replace_new.add(user)
            if len(users_replace_new) == nnew_users:
                break
print "# replace new users", len(users_replace_new)

set_test_users.difference_update(set_new_users)
set_test_users.update(users_replace_new)

# generating the new dataset
DATASET_NAME = "/sample_dataset"
out = open( globalNames.PATH + DATASET_NAME + "/nameling.testUsers", 'w')

# saving testUsers
test_list = defaultdict(list) # user => [ test_names ]
for user in set_test_users:
    name_list = train_dict[user]
    if len(name_list) <= 2:
        test_list[user] = name_list
    else:
        # random select two as test
        test_list[user] = random.sample(name_list, 2)
    
    out.write(str(user))
    for item in test_list[user]:
        out.write("\t" + item.encode('utf-8'))
    out.write("\n")    
out.close()

#saving train
print "@ Savind TRAIN"
out = open( globalNames.PATH + DATASET_NAME + "/nameling.trainData", 'w')

for user, activity, name in training:
    # user not in test and (u, i) pair not in test
    if (not user in set_test_users) or (not name in test_list[user]):
        out.write(str(user) + "\t" + activity + "\t" + name.encode('utf-8') 
                  + "\t" + 10* "0" + "\n")
out.close()