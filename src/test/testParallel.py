# -*- coding: utf-8 -*-
import sys
sys.path.append('/Users/thalesfc/workspace/naming/src')

from pickle import load
import loadData
import globalNames
from algorithms.pureSVD import PureSVDPredictor, get_top_n, get_top_n_cached
from multiprocessing import cpu_count, Pool

enter_search_dict = load(open(globalNames.PATH_DUMP_ENTERSEARCHDICT))
training, trainNames = loadData.loadTrain(globalNames.PATH_TRAINING)

testNames = loadData.loadNameList(globalNames.PATH_NAMELIST)
testUsers = loadData.loadTestUsers(globalNames.PATH_TESTUSERS)

num_factors = 50

# implicit pure SVD
svd = PureSVDPredictor(enter_search_dict, trainNames, num_factors)
outfile = open(globalNames.PATH_SUBMISSIONS + "/svd_enterSearch_" + str(num_factors), 'w')


def fun_seq(svd, U, I):
    result = []
    for user in U:
        result.append(get_top_n(svd, user, 1000, I))
    return result

def fun_pro(svd, U, I):
    pool = Pool(processes=cpu_count())
    async_result = []
    for user in U:
        async_result.append(pool.apply_async(get_top_n,(svd, user, 1000, I)))
    # waiting for results
    result = []
    for r in async_result:
        result.append(r.get())
    return result

def fun_seq_cache(svd, U, I):
    result = []
    for user in U:
        result.append(get_top_n_cached(svd, user, 1000, I))
    return result

def fun_pro_cache(svd, U, I):
    pool = Pool(processes=cpu_count())
    async_result = []
    for user in U:
        async_result.append(pool.apply_async(get_top_n_cached,(svd, user, 1000, I)))
    # waiting for results
    result = []
    for r in async_result:
        result.append(r.get())
    return result