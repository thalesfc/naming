'''
Created on May 8, 2013

@author: thalesfc
'''
from csv import reader

def loadNameList(path):
    with open(path, 'r') as f:
        data = set()
        for row in reader(f):
            data.add(unicode(''.join(row), 'utf-8').lower().strip())
        return data
    
def loadTestUsers(path):
    with open(path, 'r') as f:
        data = []
        for line in f:
            tokens = line.split('\t')
            userID = int(tokens[0])
            data.append(userID)
        return data

def loadSimilarNames(path, train_names, allowed_names):
    '''
        input: <path-to-similary-file> <names that appeared on train> 
            <names to recommend>
    
        returns:
            {name : [similar_name1, similar_name2, similar_name3 ...]
            
        where name in allowed_names
    '''
    returned = {}
    
    with open(path) as f:
        r = reader(f, delimiter='\t')
        index = 0
        
        for name, similar_name, score, _, _, _ in r:
            if index % 10000 == 0:
                print index
            index += 1
            unicode_name = unicode(name, 'utf-8').lower().strip()
            unicode_similar_name = unicode(similar_name, 'utf-8').lower().strip()

            if (unicode_name in train_names) and (unicode_similar_name in allowed_names):
                pair_2_add = (score, unicode_similar_name)
                try:
                    returned[unicode_name].append(pair_2_add)
                except KeyError:
                    returned[unicode_name] = [pair_2_add]
        return returned
    
                
        


def loadData(path):
    ''' returns:
        training -> [[user, activity, unicodeName], ...] 
        user_positions -> { user : userID}
        item_positions -> { item : itemID}
    '''
    user_positions = {}
    user_count = 0
    item_positions = {}
    item_count = 0
    
    with open(path) as f:
        r = reader(f, delimiter='\t')
        training = []
        for user, activity, name, _ in r:
            
            user = int(user)
            unicodeName = unicode(name, 'utf-8').lower().strip()
            
            training.append([user, activity, unicodeName])
            
            # setting userID
            if not user in user_positions:
                user_positions[user] = user_count
                user_count += 1
            
            #setting itemID
            if not unicodeName in item_positions:
                item_positions[unicodeName] = item_count
                item_count += 1
            
        return training, user_positions, item_positions
        
