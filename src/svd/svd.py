# -*- coding: utf-8 -*-
'''
Aline Bessa

Modified by: thalesfc@gmail.com

Baseline: latent-factor model from 
Cremonesi, Koren, and Turrin (RecSys 2010)
'''
from scipy import sparse
import numpy
from sparsesvd import sparsesvd
from heapq import heappush, nlargest

class SVD():
    def __init__(self, R, num_facs, user_positions, item_positions):
        
        self.R = R
        self.user_positions = user_positions
        self.item_positions = item_positions
        
        print "$ SVD"
        QT = self.__factorize_rating_matrix(num_facs)
        self.QT = numpy.array(QT, dtype="float32", order="C")
        
        R = sparse.csr_matrix(R, dtype="float32")
        Q = numpy.array(QT.T, dtype="float32", order="C")
        self.RQ = numpy.matrix(R * Q)
        
        
    def __factorize_rating_matrix(self, num_facs):
        _, _, qt = sparsesvd(sparse.csc_matrix(self.R), num_facs)
        return qt
    
    
    def top_n(self, user, n, allowed_items):
        if not user in self.user_positions:
            return None
        
        user_id = self.user_positions[user]
        recList = []
        W = self.RQ[user_id, :] * self.QT

        for item in allowed_items:
            try:
                item_id = self.item_positions[item]
            except KeyError:
                continue
                        
            if self.R[user_id, item_id] == 0.0:
                score = W[0, item_id]
                heappush(recList, (score, item))
        
        return nlargest(n, recList)

