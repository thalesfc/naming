# -*- coding: utf-8 -*-
'''
Aline Bessa

Modified by: thalesfc@gmail.com

Baseline: latent-factor model from 
Cremonesi, Koren, and Turrin (RecSys 2010)
'''

from scipy import sparse
from sparsesvd import sparsesvd
import numpy


class PureSVDPredictor(object): 
    def __init__(self):
        self.item_matrix_cache = {}
       
    def factorizeMatrix(self, R, num_facs, user_positions, item_positions):
        self.num_facs = num_facs
        self.R = R
        print "$ SVD"
        self.Q = self.__factorize_rating_matrix()
        self.user_positions = user_positions
        self.item_positions = item_positions
    
    def loadRfromTrain(self, training, items, num_facs):
        ''' 
        training -> {user, [name_1, name_2, name_5 ...]}
        items -> [name_1, name_2... name_n]
        '''
        
        # vars
        self.num_facs = num_facs
        self.m = len(training)
        self.n = len(items)
        
        print "$ URM ", self.m, " ", self.n
        
        # maping user, item to position
        self.user_positions, self.item_positions = self.__map_IDs_to_positions(training, items)

        # creating R
        self.R = sparse.lil_matrix((self.m, self.n))
        for user, likedItems in training.items():
            for item in likedItems:
                self.R[self.user_positions[user], self.item_positions[item]] = 1.0
                
        
        print "$ R", self.R.get_shape()

        # Compressed Sparse Row matrix: has faster dot operation
        self.R = sparse.csr_matrix(self.R)
        
        print "$ SVD"
        self.Q = self.__factorize_rating_matrix()
        

    def __map_IDs_to_positions(self, training, items):
        '''
        user and item IDs do not coincide with matrix positions, that go from 0 to m,n respectively.
        this is why we have to create this mapping
        '''
        user_pos = 0; user_map = {}
        item_pos = 0; item_map = {}

        for user in training.iterkeys():
            if not user in user_map:
                user_map[user] = user_pos
                user_pos += 1
        for item in items:
            if not item in item_map:
                item_map[item] = item_pos
                item_pos += 1
        return user_map, item_map


    def __factorize_rating_matrix(self):
        _, _, q = sparsesvd(sparse.csc_matrix(self.R), self.num_facs)
        return sparse.csr_matrix(q.T)

    
    def __get_score_lili(self, user, item):
        '''
        given a user and an item, computes a score
        for recommendation applying pureSVD
        '''
        try:
            ru = [0.0 for _ in xrange(self.n)]
            for item in self.training[user]:
                ru[self.item_positions[item]] = 1.0
            qi = self.Q[self.item_positions[item]]
        except:
            return 0.0
        return numpy.dot(numpy.array(ru), numpy.dot(self.Q, qi))
    
    
    def get_score_cached(self, user_id, item_id):
        '''
        given a user and an item, computes a score
        for recommendation applying pureSVD
        '''
        ru = self.R.getrow(user_id)
        if item_id in self.item_matrix_cache:
            Qqt = self.item_matrix_cache[item_id]
        else:
            qi = self.Q.getrow(item_id)
            Qqt = self.Q.dot(qi.T)
            self.item_matrix_cache[item_id] = Qqt
        return ru.dot(Qqt)[0, 0] 
    
    
    def get_score(self, user_id, item_id):
        '''
        given a user and an item, computes a score
        for recommendation applying pureSVD
        '''
        ru = self.R.getrow(user_id)
        qi = self.Q.getrow(item_id)
        Qqt = self.Q.dot(qi.T)
        return ru.dot(Qqt)[0, 0]