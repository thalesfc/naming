'''
Created on May 9, 2013

@author: thalesfc
'''
from globalNames import PATH_NAMELIST, PATH_TRAINING, \
    PATH_DUMPFEATURES, PATH_DUMP_NAMECOUNT  # @UnusedImport
from pickle import dump
import loadData
import globalNames

def createNameCount(outfile):
    ''' nameCount -> { name : freq } '''
    nameList = loadData.loadNameList(PATH_NAMELIST)
    training, _, _ = loadData.loadData(PATH_TRAINING)
    nameCount = {}
    for i in xrange(len(training)):
#     for i in xrange(1000):
        print i
        line = training[i]
        name = line[2]
        if name in nameList:
            try:
                nameCount[name] = nameCount[name] + 1
            except KeyError:
                nameCount[name] = 1
    dump(nameCount, open(outfile, 'wb'), 2)


def loadActivity(act_name, outfile):
    ''' 
        act_name = ENTER_SEARCH, LINK_SEARCH, LINK_CATEGORY_SEARCH, NAME_DETAILS, ADD_FAVORITE
        
        dump: {user : [name1, name2 ... ]}
    '''
    training, _, _ = loadData.loadData(PATH_TRAINING)
    data = {}
    for user, act, name in training:
        if act == act_name:
            try:
                data[user].append(name)
            except KeyError:
                data[user] = [name]
    dump(data, open(outfile, 'wb'), 2)    

def loadSimilarNames(similarity_path, outfile):
    _, _, item_positions = loadData.loadData(globalNames.PATH_TRAINING)
    allowed_names = loadData.loadNameList(globalNames.PATH_NAMELIST)
    train_names = item_positions.keys()
    
    data = loadData.loadSimilarNames(similarity_path, train_names, allowed_names)
    dump(data, open(outfile, 'wb'), 2)    

    
def main():
    print "*"
    # nameCount -> {name : tf}
#     createNameCount(PATH_DUMP_NAMECOUNT)
    
    # enter_search -> {user : [name1, name2 ...]}
#     loadActivity('ENTER_SEARCH', PATH_DUMPFEATURES + getDumpName('ENTER_SEARCH'))
#     loadActivity('LINK_SEARCH', PATH_DUMPFEATURES + getDumpName('LINK_SEARCH'))
#     loadActivity('LINK_CATEGORY_SEARCH', PATH_DUMPFEATURES + getDumpName('LINK_CATEGORY_SEARCH'))
#     loadActivity('NAME_DETAILS', PATH_DUMPFEATURES + getDumpName('NAME_DETAILS'))
#     loadActivity('ADD_FAVORITE', PATH_DUMPFEATURES + getDumpName('ADD_FAVORITE'))

    # loading names similarity
    print "de"
    loadSimilarNames(globalNames.PATH_SIMILAR_NAMES_DE, PATH_DUMPFEATURES + globalNames.get_similar_outpath('de'))
    print "fr"
    loadSimilarNames(globalNames.PATH_SIMILAR_NAMES_FR, PATH_DUMPFEATURES + globalNames.get_similar_outpath('fr'))
    print "en"
    loadSimilarNames(globalNames.PATH_SIMILAR_NAMES_EN, PATH_DUMPFEATURES + globalNames.get_similar_outpath('en'))

    print "# DOne"
   
if __name__ == "__main__":
    main()
