# -*- coding: utf-8 -*-
import sys
sys.path.append('/Users/thalesfc/workspace/naming/src')

'''
Algorithm scores name according to their similarity to 
previously user liked names
'''
import loadData
import globalNames
from pickle import load
from collections import defaultdict
from heapq import nlargest

testUsers = loadData.loadTestUsers(globalNames.PATH_TESTUSERS)
training, _, item_positions = loadData.loadData(globalNames.PATH_TRAINING)

# for each user in test, users_train has their previously liked names
users_train = {}
index = 0
print "$ Loading users_train" 
for user, _, name in training:
    if user in testUsers:
        try:
            users_train[user].add(name)
        except KeyError:
            users_train[user] = set(name)
            
    if index %10000 == 0:
        print index
    index += 1

# loading dict
print "$ loading similarity_dicts" 
de = load(open(globalNames.PATH_DUMPFEATURES + globalNames.get_similar_outpath('de')))
en = load(open(globalNames.PATH_DUMPFEATURES + globalNames.get_similar_outpath('en')))
fr = load(open(globalNames.PATH_DUMPFEATURES + globalNames.get_similar_outpath('fr')))

sim_dicts = [de, en, fr]
outfile = open(globalNames.PATH_SUBMISSIONS + "/name_similarity" , 'w')
testNames = loadData.loadNameList(globalNames.PATH_NAMELIST)

print "$ Recommending"
# for all users i shoudl recommend 
for index, user in enumerate(testUsers):
    print index, user
    outfile.write(str(user))
    recDict = defaultdict(float)
    
    try:
        set_liked_items = users_train[user]
    except KeyError:
        print " $ NEW USERS", user
        outfile.write("\n")
        continue

    # check similar items in all dictionaries
    for sim_dict in sim_dicts:
        # iterate over their previously liked items
        for liked_item in set_liked_items:
            try:
                candidates_list = sim_dict[liked_item]
            except KeyError:
                continue
            
            for (score, item_candidate) in candidates_list:
                if item_candidate in testNames:
                    recDict[item_candidate] += float(score)
    
    recList = nlargest(1000, recDict, key=recDict.get)
    if recList != None:
        for item in recList:
            outfile.write("\t" + item.encode('utf-8'))
    outfile.write("\n")

    