# -*- coding: utf-8 -*-
import sys
sys.path.append('/Users/thalesfc/workspace/naming/src')

from globalNames import PATH_DUMP_NAMECOUNT, PATH_TESTUSERS, PATH_SUBMISSIONS
from loadData import loadTestUsers
from pickle import load

testUsers = loadTestUsers(PATH_TESTUSERS)

# {name : freq}
nameCount = load(open(PATH_DUMP_NAMECOUNT))
 
sortedNameCount = sorted(nameCount, key=nameCount.get, reverse=True)
 
f = open(PATH_SUBMISSIONS + "/topPop", 'w')
 
print "Top Popular Recommender"
 
for user in testUsers:
    f.write(str(user))
    for i in xrange(1000):
        f.write("\t" + sortedNameCount[i].encode('utf-8'))
         
    f.write("\n")   