'''
Created on Jun 9, 2013

@author: thalesfc
'''
import sys
from scipy.weave.converters import default
sys.path.append('/Users/thalesfc/workspace/naming/src')

import loadData
import globalNames
from collections import defaultdict
import heapq
import operator

################################
# algorithm parameters
N = 30
################################


print 10 * "#" 
print "# UBCF"

print "# loading train"
training, user_positions, item_positions = loadData.loadData(globalNames.PATH_TRAINING)

# creating the train dict for each user
users_train = defaultdict(set)

for user, activity, name in training:
    if activity == 'ENTER_SEARCH':
        user_id = user_positions[user]
        item_id = item_positions[name]
        users_train[user_id].add(item_id)      

# Test setup
testUsers = loadData.loadTestUsers(globalNames.PATH_TESTUSERS)
testNames = loadData.loadNameList(globalNames.PATH_NAMELIST)

new_name_count = 0   
test_list = list(testNames)   
print "$ Original", len(testNames)
for item in test_list:
    if not item in item_positions:
        new_name_count += 1
        testNames.remove(item)
print "$ Deleted new names", new_name_count
print "$ Remaining", len(testNames)
test_list = None

# output setup 
outfile = open(globalNames.PATH_SUBMISSIONS + "/UBCF.txt", 'w')

# recommeding
count_nuser = 0
count_lowerl = 0


def user_based_rec(users_train, user, testNames):
    try:
        user_id = user_positions[user]
        user_set = users_train[user_id]
    except KeyError:
        return None
    similarUsers = []
    for train_user_id, train_set in users_train.items():
        if user_id == train_user_id:
            continue
        else:
            inter = set.intersection(user_set, train_set)
            diff = set.union(user_set, train_set) - inter
            score = len(inter) / (len(diff) + 1)
            heapq.heappush(similarUsers, (score, train_user_id))
        
    # get the top n users
    topn_users = heapq.nlargest(N, similarUsers)
    
    recList = defaultdict(float)
    for (score, user_id) in topn_users:
        user_set = users_train[user_id]
        for name in user_set:
            recList[name] += score
    
    # sorting names
    sorted_list = sorted(recList.iteritems(), key=operator.itemgetter(1), reverse=True)
    count = 0
    rec = []
    for (name, score) in sorted_list:
        rec.append(name)
        count += 1
        if count > 999:
            return rec    

        

for index, user in enumerate(testUsers):
    if index % 100 == 0:
        print index, user    

    outfile.write(str(user))
      
    # recommending
    recList = user_based_rec(users_train, user, testNames)
    
    if recList != None:
        if len(recList) != 1001:
            count_lowerl += 1
            
        for (item, _) in recList:
            outfile.write("\t" + item.encode('utf-8'))
    else:
        count_nuser += 1

    outfile.write("\n")

print "# New users:", count_nuser
print "# Lower lists:", count_lowerl