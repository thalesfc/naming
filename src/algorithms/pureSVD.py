import sys
sys.path.append('/Users/thalesfc/workspace/naming/src')

import loadData
import globalNames
from svd import svd
from scipy import sparse

print "# pureSVD - algorithm"

FLAG_BINARY_RATINGS = True # use rating as implicit or not


print "# loading train"
training, user_positions, item_positions = loadData.loadData(globalNames.PATH_TRAINING)

# m user and n items
m = len(user_positions)
n = len(item_positions)

# R matrix
print "# Creating R"
R = sparse.lil_matrix((m, n))

for user, activity, name in training:
    
    if FLAG_BINARY_RATINGS and activity != "ENTER_SEARCH":
        continue
    
    user_id = user_positions[user]
    item_id = item_positions[name]
    
    if( FLAG_BINARY_RATINGS ):
        R[user_id, item_id] = 1
    else:
        R[user_id, item_id] += 1

training = None
print "$ R", R.shape

num_factors = 50
singularVD = svd.SVD(R, num_factors, user_positions, item_positions)


# deleting names not in train
testUsers = loadData.loadTestUsers(globalNames.PATH_TESTUSERS)
testNames = loadData.loadNameList(globalNames.PATH_NAMELIST)
new_name_count = 0
   
test_list = list(testNames)   

print "$ Original", len(testNames)
for item in test_list:
    if not item in item_positions:
        new_name_count += 1
        testNames.remove(item)
print "$ Deleted new names", new_name_count
print "$ Remaining", len(testNames)
test_list = None

algo_name = None
if(FLAG_BINARY_RATINGS):
    algo_name = "/puresvd-bin_ES-"
else:
    algo_name = "/puresvd_"    
  
outfile = open(globalNames.PATH_SUBMISSIONS + algo_name + str(num_factors), 'w')
count_nuser = 0

for index, user in enumerate(testUsers):
    if index % 100 == 0:
        print index, user
        print "# New users:", count_nuser
      
    outfile.write(str(user))
    recList = singularVD.top_n(user, 1000, testNames)
  
    if recList != None:
        for (_, item) in recList:
            outfile.write("\t" + item.encode('utf-8'))
    else:
        count_nuser += 1
    outfile.write("\n")

print "# New users:", count_nuser