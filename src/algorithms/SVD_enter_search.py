import sys
sys.path.append('/Users/thalesfc/workspace/naming/src')

from pickle import load
import loadData
import globalNames
from pureSVD import PureSVDPredictor, get_top_n_cached
from multiprocessing import cpu_count, Pool



enter_search_dict = load(open(globalNames.PATH_DUMP_ENTERSEARCHDICT))
training, trainNames = loadData.loadTrain(globalNames.PATH_TRAINING)

testNames = loadData.loadNameList(globalNames.PATH_NAMELIST)
testUsers = loadData.loadTestUsers(globalNames.PATH_TESTUSERS)

num_factors = 50

# implicit pure SVD
svd = PureSVDPredictor(enter_search_dict, trainNames, num_factors)

# multiprocessing
pool = Pool(processes=cpu_count())
async_result = []

for user in testUsers:
        async_result.append(pool.apply_async(get_top_n_cached,(svd, user, 1000, testNames)))

# outputing
outfile = open(globalNames.PATH_SUBMISSIONS + "/svd_enterSearch_" + str(num_factors), 'w')
index = 0

for r in async_result:
    (user, recList) = r.get()
    outfile.write(str(user))
    print index, user
    index += 1
        
    if recList != None:
        for (item, _) in recList:
            outfile.write("\t" + item.encode('utf-8'))
    outfile.write("\n")
    
