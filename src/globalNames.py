'''
Created on May 8, 2013

@author: thalesfc
'''

PATH = "/Users/thalesfc/workspace/naming_data"

DATASET = "/train_dataset"
# DATASET = "/full_dataset"

PATH_SUBMISSIONS = PATH +DATASET + "/submissions"
PATH_DUMPFEATURES = PATH + DATASET +"/dumpFeatures"

PATH_DUMP_NAMECOUNT = PATH_DUMPFEATURES + "/nameCount.pkl"

def getDumpName(feature):
        return "/dict_" + feature + ".pkl"


PATH_NAMELIST = PATH + "/namelist.txt"
PATH_TESTUSERS = PATH + DATASET + "/nameling.testUsers"
PATH_TRAINING = PATH + DATASET + "/nameling.trainData"


# similarity names
def get_similar_outpath(language):
    return "/dict_" + language + ".pkl"

PATH_SIMILAR_NAMES_EN = PATH + "/COSINE_20120521-en.csv"
PATH_SIMILAR_NAMES_FR = PATH + "/COSINE_20120521-fr.csv" 
PATH_SIMILAR_NAMES_DE = PATH + "/COSINE_20120521-de.csv"  