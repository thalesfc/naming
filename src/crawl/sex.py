# -*- coding: utf-8 -*-
import sys
from Cython.Tempita import url
sys.path.append('/Users/thalesfc/workspace/naming/src')

import pickle
import globalNames
import operator
import urllib2
import math
import time
from random import randrange

base_url = 'http://nameling.net/name?reqName='
base_url = 'http://nameling.net/name/'

nameCount = pickle.load(open(globalNames.PATH_DUMP_NAMECOUNT))

sortedNames = sorted(nameCount.iteritems(), key=operator.itemgetter(1), reverse=True)

# reading previously saved data
inout = open(globalNames.PATH_SEX, 'r+a')

crawled_names = set()

for line in inout:
    tokenname, sex = line.strip().split()
    uniname = unicode(tokenname, 'utf-8')
    name = uniname.encode('utf-8')
    crawled_names.add(name)

print "# Loaded", len(crawled_names), "names."    

# doawnloading next names
for index, (uniname, score) in enumerate(sortedNames):
    name = uniname.encode('utf-8')
    print index, name, score, 
    
    if name in crawled_names:
        print
    else:
        url = base_url + name 
        print "\t" + 10 * "|" + " " + url + "\t",
        content = urllib2.urlopen(url).read()
        
        masculine = content.count('masculine')
        feminine = content.count('feminine')
        
        print "M:", masculine, "  -  F:", feminine

        if math.fabs(masculine - feminine) < 6:
            print >> sys.stderr, 15 * "$" + " Low difference", name, url
        
        sex = 'undefined'
        
        if masculine > feminine:
            sex = 'masculine'
        elif feminine > masculine:
            sex = 'feminine'
            
        inout.write(name + " " + sex + "\n")
        crawled_names.add(name)
        
        # garating to save file
        inout.flush()
        
        sleep_time = randrange(10, 30)
        time.sleep(sleep_time)
    
        